package main

import (
	"archive/zip"
	"bufio"
	"bytes"
	"fmt"
	format "fmt"
	"html/template"
	"io"
	"log"
	"mime/multipart"
	"net/http"
	"os"
	"os/exec"
	"path"
	"path/filepath"
	"regexp"
	"runtime"
	"strings"
)

const dataFolder = "../../log_data/"
const dataFolder_raw = "../../"

type findWord struct {
	checkString     string
	checkDesc       string
	CheckColor      string
	CheckRegComplie *regexp.Regexp
}

var myFindWord = [...]findWord{
	{"Tigris daemon start", "Boot Start", "blue", nil},
	{"Send SYS_POST_BOOT_COMPLETED", "BOOT_COMPLETED", "red", nil},
	//{"ApplicationManagerService], readies", "App Service Ready", "red"},
	//{"PowerManagerService], readies", "Power Service Ready", "red"},
	//{"AlarmManagerService], readies", "Alarm Service Ready", "red"},
	{"Send SYS_POST_BOOT_COMPLETED_PRE", "BOOT_PRE", "blue", nil},
	{" E ", "ERROR", "red", nil},
}

func main() {

	var mux *http.ServeMux
	mux = http.NewServeMux()

	if runtime.GOOS == "windows" {
		format.Println("Linux")
	}

	for _, myFindWordLoop := range myFindWord {
		myFindWordLoop.CheckRegComplie, _ = regexp.Compile(myFindWordLoop.checkString)
	}

	if !fileExists(dataFolder) {
		os.Mkdir(dataFolder, os.ModeDir)
	}

	mux.HandleFunc("/hello", func(writer http.ResponseWriter, r *http.Request) {
		format.Fprintln(writer, "HandleFunc Hello")
	})
	var hf http.HandlerFunc
	hf = func(writer http.ResponseWriter, request *http.Request) {
		format.Fprintln(writer, "HandlerFunc 형을 정의 -> Handle 메서드에 전달")
	}
	mux.Handle("/hf", hf)
	mux.HandleFunc("/upload", upload)
	mux.HandleFunc("/", index)
	mux.HandleFunc("/test", indextest)
	var myHandler = new(MyHandle)

	mux.Handle("/handle", myHandler)
	var server *http.Server
	server = &http.Server{}
	server.Addr = ":11180"
	server.Handler = mux
	server.ListenAndServe()
}

type indexdata struct {
	MarkCount    int64
	MaxMarkCount int64
	Name0        string
	Name1        string
	Number1      [10]int64

	MainNumber         []int64
	MainString         []string
	MainDescString     []string
	MainLinenumber     []int64
	MainColor          []string
	MainFileName       []string
	MainLineatFileName []int64

	RadioNumber     []int64
	RadioString     []string
	RadioLinenumber []int64
	RadioColor      []int64
	RadioFileName   []string
}

type MyHandle struct {
}

func (this *MyHandle) ServeHTTP(writer http.ResponseWriter, request *http.Request) {
	format.Fprintln(writer, "http.Handler 형 인터페이스를 구현 한 객체를 http.Handle에 전달")
}

func index(writer http.ResponseWriter, request *http.Request) {
	var t *template.Template
	p := indexdata{Name0: "eeeeee", Name1: "testtest"}
	p.Number1[0] = 100
	p.Number1[1] = 200
	t, _ = template.ParseFiles("../template/index.html")
	t.Execute(writer, p)
}

func indextest(writer http.ResponseWriter, request *http.Request) {
	var t *template.Template
	p := indexdata{Name0: "eeeeee", Name1: "testtest"}
	t, _ = template.ParseFiles("../template/index_test.html")
	t.Execute(writer, p)
}

func mainRead(uploadedFileName string, p *indexdata) {
	libRegEx, e := regexp.Compile("^main.log.*$")
	if e != nil {
		log.Fatal(e)
	}

	p.MaxMarkCount = int64(0)
	var readlineCount = int64(0)
	var findreadlineCount = int64(0)

	e = filepath.Walk(dataFolder+FilenameWithoutExtension(uploadedFileName), func(path string, info os.FileInfo, err error) error {
		if err == nil && libRegEx.MatchString(info.Name()) {
			println(path)

			readFile, err := os.Open(path)
			if err != nil {
				log.Fatalf("failed to open file: %s", err)
			} else {
				println("Open File %s", info.Name())
			}

			fileScanner := bufio.NewScanner(readFile)
			fileScanner.Split(bufio.ScanLines)

			for fileScanner.Scan() {
				//println(fileScanner.Text())
				//println("readlineCount= ", readlineCount)
				readlineCount = readlineCount + 1
				for _, myFindWordLoop := range myFindWord {
					//println(myFindWordLoop.checkString)
					if strings.Contains(fileScanner.Text(), myFindWordLoop.checkString) {
						p.MainNumber[findreadlineCount] = readlineCount
						p.MainString[findreadlineCount] = fileScanner.Text()
						p.MainColor[findreadlineCount] = myFindWordLoop.CheckColor
						p.MainDescString[findreadlineCount] = myFindWordLoop.checkDesc
						if findreadlineCount < (p.MarkCount - 1) {
							findreadlineCount = findreadlineCount + 1
							//println("findreadlineCount=%d", findreadlineCount)
							break
						}
					}
				}
			}
			readFile.Close()
		}
		return nil
	})
	p.MaxMarkCount = readlineCount
	p.MainNumber[findreadlineCount] = -1
	p.MainLinenumber[findreadlineCount] = -1
}

func upload(writer http.ResponseWriter, r *http.Request) {
	if r.Method != "POST" {
		format.Fprintln(writer, "잘못된 사용입니다.")
		return
	}
	var file multipart.File
	var fileHeader *multipart.FileHeader
	var e error
	var uploadedFileName string
	file, fileHeader, e = r.FormFile("image")

	if e != nil {
		format.Fprintln(writer, "파일 업로드를 확인할 수 없습니다.")
		return
	}

	uploadedFileName = fileHeader.Filename
	if !isItSupportFilename(uploadedFileName) {
		format.Fprintln(writer, "Not Supported File")
		return
	}

	var saveImage *os.File
	saveImage, e = os.Create(dataFolder_raw + uploadedFileName)

	if e != nil {
		format.Fprintln(writer, "서버 측에서 파일 확보 할 수 없습니다.")
		return
	}

	defer saveImage.Close()
	defer file.Close()
	size, e := io.Copy(saveImage, file)
	if e != nil {
		format.Println(e)
		format.Println("파일생성에 실패하였습니다.")
		os.Exit(1)
	}
	format.Println(size)

	Unzip(dataFolder_raw+uploadedFileName, dataFolder+FilenameWithoutExtension(uploadedFileName))

	p := indexdata{Name0: "eeeeee", Name1: "testtest"}
	p.MarkCount = 20000
	p.MaxMarkCount = 0
	p.Number1[0] = 100
	p.Number1[1] = 200

	p.MainString = make([]string, p.MarkCount)
	p.MainFileName = make([]string, p.MarkCount)
	p.MainLineatFileName = make([]int64, p.MarkCount)
	p.MainDescString = make([]string, p.MarkCount)
	p.MainNumber = make([]int64, p.MarkCount)
	p.MainLinenumber = make([]int64, p.MarkCount)
	p.MainColor = make([]string, p.MarkCount)

	p.RadioString = make([]string, p.MarkCount)
	p.RadioFileName = make([]string, p.MarkCount)
	p.RadioNumber = make([]int64, p.MarkCount)
	p.RadioLinenumber = make([]int64, p.MarkCount)
	p.RadioColor = make([]int64, p.MarkCount)

	mainRead(uploadedFileName, &p)
	if e != nil {
		log.Fatal(e)
	}

	/*
		parsing data setting
	*/
	var t *template.Template
	t, _ = template.ParseFiles("../template/analysis.html")
	t.Execute(writer, p)
}

func FilenameWithoutExtension(fn string) string {
	return strings.TrimSuffix(fn, path.Ext(fn))
}

func isItSupportFilename(uploadedFileName string) bool {

	zipindex := strings.LastIndex(uploadedFileName, ".zip")
	sevenzipindex := strings.LastIndex(uploadedFileName, ".7z")
	// tarindex := strings.LastIndex(uploadedFileName, ".tar")
	gzindex := strings.LastIndex(uploadedFileName, ".gz")
	if (zipindex != -1) || (sevenzipindex != -1) || /* (tarindex != -1) || */ (gzindex != -1) {
		return true
	}
	return false
}

func runCommand(name string, args ...string) (string, error) {
	cmd := exec.Command(name, args...)
	// Stdout pipe for reading the generated output.
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	// Run the script.
	if err := cmd.Run(); err != nil {
		c := name
		if len(args) > 0 {
			c += " " + strings.Join(args, " ")
		}
		fmt.Println("error " + stderr.String())
		return "", fmt.Errorf("failed to run command %q:\n  %v\n  %s", c, err, stderr.String())
	}

	return stdout.String(), nil
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func Unzip(src string, dest string) ([]string, error) {

	var filenames []string

	r, err := zip.OpenReader(src)
	if err != nil {
		return filenames, err
	}
	defer r.Close()

	for _, f := range r.File {

		// Store filename/path for returning and using later on
		fpath := filepath.Join(dest, f.Name)

		// Check for ZipSlip. More Info: http://bit.ly/2MsjAWE
		if !strings.HasPrefix(fpath, filepath.Clean(dest)+string(os.PathSeparator)) {
			return filenames, fmt.Errorf("%s: illegal file path", fpath)
		}

		filenames = append(filenames, fpath)

		if f.FileInfo().IsDir() {
			// Make Folder
			os.MkdirAll(fpath, os.ModePerm)
			continue
		}

		// Make File
		if err = os.MkdirAll(filepath.Dir(fpath), os.ModePerm); err != nil {
			return filenames, err
		}

		outFile, err := os.OpenFile(fpath, os.O_WRONLY|os.O_CREATE|os.O_TRUNC, f.Mode())
		if err != nil {
			return filenames, err
		}

		rc, err := f.Open()
		if err != nil {
			return filenames, err
		}

		_, err = io.Copy(outFile, rc)

		// Close the file without defer to close before next iteration of loop
		outFile.Close()
		rc.Close()

		if err != nil {
			return filenames, err
		}
	}
	return filenames, nil
}

/*
func upload (writer http.ResponseWriter, r *http.Request) {
    if  (r.Method != "POST") {
        format.Fprintln(writer, "許可したメソッドとはことなります。");
        return;
    }
    var file multipart.File;
    var fileHeader *multipart.FileHeader;
    var e error;
    var uploadedFileName string;
    file , fileHeader , e = r.FormFile ("image");
    format.Printf("%T", file);
    if (e != nil) {
        format.Fprintln(writer, "ファイルアップロードを確認できませんでした。");
        return;
    }
    uploadedFileName = fileHeader.Filename;
    var saveImage *os.File;
    saveImage, e = os.Create("./" + uploadedFileName);
    if (e != nil) {
        format.Fprintln(writer, "サーバ側でファイル確保できませんでした。");
        return;
    }
    defer saveImage.Close();
    defer file.Close();
    size, e := io.Copy(saveImage, file);
    if (e != nil) {
        format.Println(e);
        format.Println("アップロードしたファイルの書き込みに失敗しました。");
        os.Exit(1);
    }
    format.Println("書き込んだByte数=>");
    format.Println(size);
    format.Fprintf(writer, "文字列HTTPとして出力させる");
}
*/
